package com.ghintech.prim.agrosilos.weightcontrol.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;

import com.ghintech.prim.agrosilos.weightcontrol.model.MXXTicketVigilancia;

public class Ticket_Vigilancia implements IColumnCallout{
		@Override
		public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,
				Object oldValue) {			
			Integer m_XX_Ticket_Vigilancia_ID = (Integer)value;
			if (m_XX_Ticket_Vigilancia_ID == null || m_XX_Ticket_Vigilancia_ID.intValue() == 0)
				return "";
		    MXXTicketVigilancia tVigilancia = new MXXTicketVigilancia(ctx, m_XX_Ticket_Vigilancia_ID.intValue(), null);
		    mTab.setValue("C_BPartner_ID", tVigilancia.getC_BPartner_ID());
		    mTab.setValue("M_Shipper_ID", tVigilancia.get_ValueAsInt("M_Shipper_ID"));
		    mTab.setValue("XX_Conductor_ID", tVigilancia.get_ValueAsInt("XX_Conductor_ID"));
		    mTab.setValue("XX_Vehiculo_ID", tVigilancia.get_ValueAsInt("XX_Vehiculo_ID"));
		    Integer m_C_BPartner_ID =  tVigilancia.getC_BPartner_ID();
		    MBPartner socio = new MBPartner(ctx, m_C_BPartner_ID.intValue(), null);
		    mTab.setValue("IsInclude", socio.get_Value("IsInclude"));
		    
			return null;
		}
	}