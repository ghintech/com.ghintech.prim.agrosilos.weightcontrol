package com.ghintech.prim.agrosilos.weightcontrol.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;

public class AsignDocType implements IColumnCallout{
		@Override
		public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value,
				Object oldValue) {			
			Integer v_C_DocTypeTarget_ID = (Integer)value;
			if (v_C_DocTypeTarget_ID == null || v_C_DocTypeTarget_ID.intValue() == 0)
				return "";
		    
		    mTab.setValue("C_Doctype_ID", v_C_DocTypeTarget_ID);		    
			return null;
		}
	}