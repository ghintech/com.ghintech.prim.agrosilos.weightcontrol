package com.ghintech.prim.agrosilos.weightcontrol.model;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

public class ModelFactory implements IModelFactory {

	@Override
	public Class<?> getClass(String tableName) {
        if ("C_Order".equals(tableName)) {
            return MXXTicketVigilancia.class;
        }
     /*   if ("PP_Order".equals(tableName)) {
           return MPPOrder.class;
        }*/
        return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
        if ("C_Order".equals(tableName)) {
            return new MXXTicketVigilancia(Env.getCtx(), Record_ID, trxName);
        }
       /* if ("PP_Order".equals(tableName)) {
            return new MPPOrder(Env.getCtx(), Record_ID, trxName);
        }*/
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
        if ("C_Order".equals(tableName)) {
            return new MXXTicketVigilancia(Env.getCtx(), rs, trxName);
        }
       /* if ("PP_Order".equals(tableName)) {
            return new MPPOrder(Env.getCtx(), rs, trxName);
        }*/
		return null;
	}

}
