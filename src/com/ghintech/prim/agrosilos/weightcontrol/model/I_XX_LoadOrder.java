/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package com.ghintech.prim.agrosilos.weightcontrol.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for XX_LoadOrder
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_XX_LoadOrder 
{

    /** TableName=XX_LoadOrder */
    public static final String Table_Name = "XX_LoadOrder";

    /** AD_Table_ID=1000016 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_DocTypeOrder_ID */
    public static final String COLUMNNAME_C_DocTypeOrder_ID = "C_DocTypeOrder_ID";

	/** Set Tipo de Orden.
	  * Tipo de Orden de Venta
	  */
	public void setC_DocTypeOrder_ID (int C_DocTypeOrder_ID);

	/** Get Tipo de Orden.
	  * Tipo de Orden de Venta
	  */
	public int getC_DocTypeOrder_ID();

	public I_C_DocType getC_DocTypeOrder() throws RuntimeException;

    /** Column name Capacity */
    public static final String COLUMNNAME_Capacity = "Capacity";

	/** Set Capacity	  */
	public void setCapacity (BigDecimal Capacity);

	/** Get Capacity	  */
	public BigDecimal getCapacity();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name DateDoc */
    public static final String COLUMNNAME_DateDoc = "DateDoc";

	/** Set Document Date.
	  * Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc);

	/** Get Document Date.
	  * Date of the Document
	  */
	public Timestamp getDateDoc();

    /** Column name DateFinish */
    public static final String COLUMNNAME_DateFinish = "DateFinish";

	/** Set Finish Date.
	  * Finish or (planned) completion date
	  */
	public void setDateFinish (Timestamp DateFinish);

	/** Get Finish Date.
	  * Finish or (planned) completion date
	  */
	public Timestamp getDateFinish();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DocumentNo */
    public static final String COLUMNNAME_DocumentNo = "DocumentNo";

	/** Set Document No.
	  * Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo);

	/** Get Document No.
	  * Document sequence number of the document
	  */
	public String getDocumentNo();

    /** Column name DriverDescription */
    public static final String COLUMNNAME_DriverDescription = "DriverDescription";

	/** Set Driver Description.
	  * Optional short description of the record
	  */
	public void setDriverDescription (String DriverDescription);

	/** Get Driver Description.
	  * Optional short description of the record
	  */
	public String getDriverDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsDelivered */
    public static final String COLUMNNAME_IsDelivered = "IsDelivered";

	/** Set Delivered	  */
	public void setIsDelivered (boolean IsDelivered);

	/** Get Delivered	  */
	public boolean isDelivered();

    /** Column name IsInvoiced */
    public static final String COLUMNNAME_IsInvoiced = "IsInvoiced";

	/** Set Invoiced.
	  * Is this invoiced?
	  */
	public void setIsInvoiced (boolean IsInvoiced);

	/** Get Invoiced.
	  * Is this invoiced?
	  */
	public boolean isInvoiced();

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_LocatorTo_ID */
    public static final String COLUMNNAME_M_LocatorTo_ID = "M_LocatorTo_ID";

	/** Set Locator To.
	  * Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID);

	/** Get Locator To.
	  * Location inventory is moved to
	  */
	public int getM_LocatorTo_ID();

	public I_M_Locator getM_LocatorTo() throws RuntimeException;

    /** Column name M_Shipper_ID */
    public static final String COLUMNNAME_M_Shipper_ID = "M_Shipper_ID";

	/** Set Shipper.
	  * Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID);

	/** Get Shipper.
	  * Method or manner of product delivery
	  */
	public int getM_Shipper_ID();

	public I_M_Shipper getM_Shipper() throws RuntimeException;

    /** Column name M_Warehouse_ID */
    public static final String COLUMNNAME_M_Warehouse_ID = "M_Warehouse_ID";

	/** Set Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID);

	/** Get Warehouse.
	  * Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID();

	public I_M_Warehouse getM_Warehouse() throws RuntimeException;

    /** Column name ShipDate */
    public static final String COLUMNNAME_ShipDate = "ShipDate";

	/** Set Ship Date.
	  * Shipment Date/Time
	  */
	public void setShipDate (Timestamp ShipDate);

	/** Get Ship Date.
	  * Shipment Date/Time
	  */
	public Timestamp getShipDate();

    /** Column name TotalWeight */
    public static final String COLUMNNAME_TotalWeight = "TotalWeight";

	/** Set TotalWeight	  */
	public void setTotalWeight (BigDecimal TotalWeight);

	/** Get TotalWeight	  */
	public BigDecimal getTotalWeight();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name VehicleDescription */
    public static final String COLUMNNAME_VehicleDescription = "VehicleDescription";

	/** Set Vehicle Description.
	  * Optional short description of the record
	  */
	public void setVehicleDescription (String VehicleDescription);

	/** Get Vehicle Description.
	  * Optional short description of the record
	  */
	public String getVehicleDescription();

    /** Column name XX_Annulled */
    public static final String COLUMNNAME_XX_Annulled = "XX_Annulled";

	/** Set Annulled	  */
	public void setXX_Annulled (String XX_Annulled);

	/** Get Annulled	  */
	public String getXX_Annulled();

    /** Column name XX_Conductor_ID */
    public static final String COLUMNNAME_XX_Conductor_ID = "XX_Conductor_ID";

	/** Set Conductor	  */
	public void setXX_Conductor_ID (int XX_Conductor_ID);

	/** Get Conductor	  */
	public int getXX_Conductor_ID();

    /** Column name XX_LoadOrder_ID */
    public static final String COLUMNNAME_XX_LoadOrder_ID = "XX_LoadOrder_ID";

	/** Set Load Order	  */
	public void setXX_LoadOrder_ID (int XX_LoadOrder_ID);

	/** Get Load Order	  */
	public int getXX_LoadOrder_ID();

    /** Column name XX_NetWeight */
    public static final String COLUMNNAME_XX_NetWeight = "XX_NetWeight";

	/** Set Net Weight	  */
	public void setXX_NetWeight (BigDecimal XX_NetWeight);

	/** Get Net Weight	  */
	public BigDecimal getXX_NetWeight();

    /** Column name XX_Vehicle_UOM_ID */
    public static final String COLUMNNAME_XX_Vehicle_UOM_ID = "XX_Vehicle_UOM_ID";

	/** Set Vehicle Unit Measure	  */
	public void setXX_Vehicle_UOM_ID (int XX_Vehicle_UOM_ID);

	/** Get Vehicle Unit Measure	  */
	public int getXX_Vehicle_UOM_ID();

	public I_C_UOM getXX_Vehicle_UOM() throws RuntimeException;

    /** Column name XX_Vehiculo_ID */
    public static final String COLUMNNAME_XX_Vehiculo_ID = "XX_Vehiculo_ID";

	/** Set Car	  */
	public void setXX_Vehiculo_ID (int XX_Vehiculo_ID);

	/** Get Car	  */
	public int getXX_Vehiculo_ID();

    /** Column name XX_Work_UOM_ID */
    public static final String COLUMNNAME_XX_Work_UOM_ID = "XX_Work_UOM_ID";

	/** Set Unit of Work	  */
	public void setXX_Work_UOM_ID (int XX_Work_UOM_ID);

	/** Get Unit of Work	  */
	public int getXX_Work_UOM_ID();

	public I_C_UOM getXX_Work_UOM() throws RuntimeException;

    /** Column name XXIsBulk */
    public static final String COLUMNNAME_XXIsBulk = "XXIsBulk";

	/** Set Is Bulk	  */
	public void setXXIsBulk (boolean XXIsBulk);

	/** Get Is Bulk	  */
	public boolean isXXIsBulk();

    /** Column name XXIsDriverReleased */
    public static final String COLUMNNAME_XXIsDriverReleased = "XXIsDriverReleased";

	/** Set Driver Released	  */
	public void setXXIsDriverReleased (boolean XXIsDriverReleased);

	/** Get Driver Released	  */
	public boolean isXXIsDriverReleased();

    /** Column name XXIsInternalLoad */
    public static final String COLUMNNAME_XXIsInternalLoad = "XXIsInternalLoad";

	/** Set Is Internal Load	  */
	public void setXXIsInternalLoad (boolean XXIsInternalLoad);

	/** Get Is Internal Load	  */
	public boolean isXXIsInternalLoad();

    /** Column name XXIsMoved */
    public static final String COLUMNNAME_XXIsMoved = "XXIsMoved";

	/** Set Is Moved	  */
	public void setXXIsMoved (boolean XXIsMoved);

	/** Get Is Moved	  */
	public boolean isXXIsMoved();

    /** Column name XXIsVehicleReleased */
    public static final String COLUMNNAME_XXIsVehicleReleased = "XXIsVehicleReleased";

	/** Set Vehicle Released	  */
	public void setXXIsVehicleReleased (boolean XXIsVehicleReleased);

	/** Get Vehicle Released	  */
	public boolean isXXIsVehicleReleased();

    /** Column name XXIsWeightRegister */
    public static final String COLUMNNAME_XXIsWeightRegister = "XXIsWeightRegister";

	/** Set Is Weight Register	  */
	public void setXXIsWeightRegister (boolean XXIsWeightRegister);

	/** Get Is Weight Register	  */
	public boolean isXXIsWeightRegister();
}
